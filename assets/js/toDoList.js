// All "console.log" are comments just for you
//  to see a message when an event is executed

// ===============
//	Variables
// ===============

const taskList = $('#task-list')
const ul = $('#task-list').html('<ul>');
const task = $('#task').val();


// ===============
//	Functions
// ===============

// Add task to the list
const addTask = (e) => {
	e.preventDefault();

	// Task name
	let task = $('#task').val();

	// Add new task to list if it has at least one character
	if (task != '') {
		const newTask = ul.append(`<li> ${task} <a></a></li>`);

		console.log(`'${task}' added successfully`);

	}
	else {
		alert('Task must have at least 1 (one) character');

		console.log('Error: You should write at least 1 (one) character')
	}

	// Create erase button
	const erase = $('li').children('a');
	erase.addClass('erase-task');
	erase.text('X');

// Strike out task
// -------------------------------------------
// Unbind event to prevent trigger event twice
	$('li').unbind('click');
	
	$('li').click(function() {
		$(this).toggleClass('strike');
	})

// Erase task from textarea after submiting one
	$('#task').val()

	$('#task').val('');
	$('#task').focus();

// Remove task from list
		erase.unbind('click')

		// Remove task from list
		erase.click(function() {
			// task's value
			task = $(this).parent().text();
			// Delete <li></li>
			$(this).parent().remove();
			// Delete from local storage
			removeLocalStorage(task);

			// console.log(`The task '${task}' was removed successfully`)
		});

// Add task to Local Storage
	addTaskLocalStorage(task);

}


// Remove all task in the list
const clearList =  (e) => {
	e.preventDefault();
	var clean = confirm('Are you sure you want to clear the list?');

	if (clean == true) $('#task-list').children().remove()
	// Clear all from Local Storage
	localStorage.clear()
}


// Add task to Local Storage
// _________________________
const addTaskLocalStorage = (task) => {
	let tasks;

	tasks = getTasksLocalStorage();

	tasks.push(task);

	localStorage.setItem('tasks', JSON.stringify(tasks));
}


// Get task from Local Storage
// ___________________________
const getTasksLocalStorage = () => {
	let tasks;
	if (localStorage.getItem('tasks') === null) {
		tasks = [];
	} else {
		tasks = JSON.parse(localStorage.getItem('tasks'));
	}
	return tasks;
}

// Load Local Storage when refreshing page
// ___________________________
const loadLocalStorage = () => {
	let tasks;

	tasks = getTasksLocalStorage();

	tasks.forEach(function(task) {
		// Create tasks list
		const newTask = ul.append(`<li> ${task} <a></a></li>`);

		// Create erase button
		const erase = $('li').children('a');
		erase.addClass('erase-task');
		erase.text('X');

		// Strike out task
		// -------------------------------------------
		// Unbind event to prevent trigger event twice
		$('li').unbind('click');
		
		$('li').click(function() {
			$(this).toggleClass('strike');
		})
		// Remove task from list
		erase.unbind('click')

		// Remove task from list
		erase.click(function() {
			// task's value
			task = $(this).parent().text();
			// Delete <li></li>
			$(this).parent().remove();
			// Delete from LocalStorage
			removeLocalStorage(task);
			// console.log(`The task '${task}' was removed successfully`)
		});


	});
}

// Delete task from Local Storage
// ___________
function removeLocalStorage(task) {
    let tasks, taskToErase;
    
    // Deletes "x" from task
    taskToErase = task.substring(0, task.length - 1)
    // Trim to avoid unwanted spaces
    taskToErase = $.trim(taskToErase)


    tasks = getTasksLocalStorage();

    tasks.forEach(function(task, index){
        if(taskToErase === task) {
            tasks.splice(index, 1)
        }
    })
 
    localStorage.setItem('tasks', JSON.stringify(tasks))
}






// ===============
//	Events
// ===============

// Submit task
// ___________

$('#formulary').submit(addTask);

// Clear list
// ___________

$('#reset').click(clearList);

// Load Local Storage
// ___________
$('document').ready(loadLocalStorage);
